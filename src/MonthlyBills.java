//Sherwayne Palmer 738019
//CSAT
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MonthlyBills   {
	
	//global variables
	static ArrayList<Customer> _customers = new ArrayList<Customer>();
	static Customer _searchedCust;
	DateFormat _dtf = new SimpleDateFormat("EEEE,dd MMM,yyyy");
	
	public static void main(String[] args) {
		
		Date dt = new Date(); //instantiate date object
		
		//create internet object
		Internet interBill = new Internet(); 
		//set values to internet properties
		interBill.setId(1);
		interBill.setBillDate(dt);
		interBill.setBillType("Internet");
		interBill.setProviderName("Rogers");
		interBill.setGBused(500);
		interBill.setTotalBillAmount(1000);
		
		//create hydro object
		Hydro hydroBill = new Hydro();
		hydroBill.setId(2);
		hydroBill.setBillDate(dt);
		hydroBill.setBillType("HydroOne");
		hydroBill.setAgencyName("WayneCorp");
		hydroBill.setUnitConsumed(29);
		hydroBill.setTotalBillAmount(500);
		
		//create mobile object
		Mobile mobileBill = new Mobile();
		//set values to mobile object
		mobileBill.setId(3);
		mobileBill.setBillDate(dt);
		mobileBill.setBillType("Mobile");
		mobileBill.setManufacturerName("Galaxy Samsung Inc");
		mobileBill.setPlanName("Prepaid Talk + Text Plan");
		mobileBill.setMobileNumber("+4169855153");
		mobileBill.setGBused(5);
		mobileBill.setMinuteUsed(356);
		mobileBill.setTotalBillAmount(800);
		
		//create array list to store bills
		ArrayList<Object> billsList1 =  new ArrayList();
		billsList1.add(interBill);
		billsList1.add(hydroBill);

		//create array list to store bills
		ArrayList<Object> billsList2 =  new ArrayList();
		billsList2.add(hydroBill);
		billsList2.add(interBill);
		billsList2.add(mobileBill);
		
		//create array list to store bills
		ArrayList<Object> billsList3 =  new ArrayList();
		billsList3.add(interBill);
		
		
		//create customer objects
		Customer customer1 = new Customer(1,"Sherwayne","Palmer","sherwaynepalmer@hotmail.com",billsList1);
		Customer customer2 = new Customer(2,"Deivid","Mafra","deividmafr4@gmail.com",billsList2);
		Customer customer3 = new Customer(3,"Diego","Perez","dapohp@gmail.com");
           
		//add customers to list
		_customers.add(customer1);
        _customers.add(customer2);
        _customers.add(customer3);
        
        //create bills object
        MonthlyBills bill = new MonthlyBills();
        
        System.out.println("Enter customer ID to search or 'all' to get all customers:");
        Scanner scanner = new Scanner(System.in); //create instance of scanner class
		String userInput = scanner.nextLine(); //gets user input

		//search for customer by id
        _searchedCust = bill.getCustomerById(userInput);
        
         //checks condition for searched customer otherwise get all customers      
        if(_searchedCust != null)
		{
        	_customers.clear();
        	_customers.add(_searchedCust);
        	bill.display(); //calls display for interface
        	
		}
		else if(_searchedCust == null && !userInput.equals("all"))
		{
			System.out.println("This customer does not exist"); 
			return;
		}
		else if(userInput.equals("all"))
		{
			_customers.clear();
			_customers.add(customer1);
	        _customers.add(customer2);
	        _customers.add(customer3);
			bill.display(); 
		}
        
              
	}
	
	//implements interface method
	public void display() 
    {   
			List<Customer> sortedCustById = _customers.stream()
      		  .sorted(Comparator.comparing(Customer::getId))
      		  .collect(Collectors.toList());
        
        for(Customer cust: sortedCustById)
		{
        	System.out.println("Customer Id:"+cust.getId()); 
            System.out.println("Customer FullName:"+cust.getFirstName()+" "+cust.getLastName());
            System.out.println("Customer Email ID:"+cust.getEmailID());
            System.out.println("---- Bill Information ----");
            System.out.println("**********************************************");
            
            if(cust.getBillList() != null)
            {
            	
            //sorts customer bill by bill amount
            ArrayList<Bill> sortedCustBill = (ArrayList<Bill>) cust.getBillList().stream()
          		  .sorted(Comparator.comparing(Bill::getTotalBillAmount))
          		  .collect(Collectors.toList());
            

            for(Bill bill: sortedCustBill)
    		{
	           //checks bill type does some nonesense
	            switch(bill.getBillType())
	            {
	            case "HydroOne":
	            	Hydro hydroBill = (Hydro)bill;
	            	
	            	System.out.println("Bill Id:"+hydroBill.getId()); 
	 	            System.out.println("Bill Date:"+_dtf.format(hydroBill.getBillDate())); 
	 	            System.out.println("Bill Type:"+hydroBill.getBillType()); 
	 	            System.out.println("Bill Amount:"+hydroBill.getTotalBillAmount());
	 	            System.out.println("Company Name:"+hydroBill.getAgencyName());
	 	            System.out.println("Unit Consumed:"+hydroBill.getUnitConsumed());
	         
	            	break;
	            case "Mobile":
	            	Mobile mobileBill = (Mobile)bill;
	            	
	            	 	System.out.println("Bill Id:"+mobileBill.getId()); 
		 	            System.out.println("Bill Date:"+_dtf.format(mobileBill.getBillDate())); 
		 	            System.out.println("Bill Type:"+mobileBill.getBillType()); 
		 	            System.out.println("Bill Amount:"+mobileBill.getTotalBillAmount());
		 	            System.out.println("Manufacturer Name:"+mobileBill.getManufacturerName());
		 	            System.out.println("Plan Name:"+mobileBill.getPlanName());
		 	            System.out.println("Mobile Number:"+mobileBill.getMobileNumber());
		 	            System.out.println("Internet Usage:"+mobileBill.getGBused());
		 	            System.out.println("Minutes Usage:"+mobileBill.getMinuteUsed());
		 	            
	            	break;
	            case "Internet":
	            	    Internet interBill = (Internet)bill;
	            	    System.out.println("Bill Id:"+interBill.getId()); 
		 	            System.out.println("Bill Date:"+_dtf.format(interBill.getBillDate())); 
		 	            System.out.println("Bill Type:"+interBill.getBillType()); 
		 	            System.out.println("Bill Amount:"+interBill.getTotalBillAmount());
		 	            System.out.println("Provider Name:"+interBill.getProviderName());
		 	            System.out.println("Internet Usage:"+interBill.getGBused());
	 	           
	            	break;
	            default:
	            	
	            	break;
	            }
	            
	            System.out.println("*********************************************");
	            int payAmount = bill.getTotalBillAmount();
	            cust.setTotalPayAmount(payAmount);
	            //cust.totalPayAmount +=bill.totalBillAmount; //sums bill amount
    		}
            
            System.out.println("Total Bill Amount to Pay:"+cust.getTotalPayAmount());
            System.out.println("*********************************************");
            System.out.println(""); 
            cust.setTotalPayAmount(0);
            
            }
            else
            {
            	System.out.println("~~~NOTE: This customer has no bills"); //condition if customer has no bills 
            }
		}
    } 
	
	//function to search by customer id
	public Customer getCustomerById(String customerId)
	{
		boolean isInteger = false;
		try {
			Integer.parseInt(customerId);
			isInteger = true;
		}catch(Exception e){
			isInteger = false;
		}
		
		if(isInteger == true)
		{
			for(Customer cust: _customers)
			{
				if(cust.getId() == Integer.parseInt(customerId))
					{
					 return cust;
					}
			}
		}
		
		return null;
	}
	
}
