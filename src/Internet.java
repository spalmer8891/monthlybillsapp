
public class Internet extends Bill{

	private String providerName;
	private int GBused;
	
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public int getGBused() {
		return GBused;
	}
	public void setGBused(int gBused) {
		GBused = gBused;
	}
	
}
