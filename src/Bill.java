//Sherwayne Palmer 738019
//CSAT
import java.util.Date;

public class Bill{

	private int billID;
	private Date billDate;
	private String billType;
	private int totalBillAmount;
	
	public int getId() {
		return billID;
	}
	public void setId(int id) {
		this.billID = id;
	}
	public Date getBillDate() {
		return billDate;
	}
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	public int getTotalBillAmount() {
		return totalBillAmount;
	}
	public void setTotalBillAmount(int totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}
	
	
	
}
