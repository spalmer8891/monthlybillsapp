//Sherwayne Palmer 738019
//CSAT
import java.util.ArrayList;

public class Customer {

	private int customerID;
	private String firstName;
	private String lastName;
	private String fullName;
	private String emailID;
	private ArrayList billList;
	private int totalPayAmount;
	
	public Customer()
	{
		
	}
	
	Customer(int CustomerID, String firstName,String lastName, String emailID)
	{
		this.customerID = CustomerID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailID = emailID;
	}
	
	Customer(int CustomerID, String firstName,String lastName, String emailID,ArrayList billList)
	{
		this.customerID = CustomerID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailID = emailID;
		this.billList = billList;
	}
	
	
	public int getId() {
		return customerID;
	}
	public void setId(int id) {
		this.customerID = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public ArrayList getBillList() {
		return billList;
	}
	public void setBillList(ArrayList billList) {
		this.billList = billList;
	}
	public int getTotalPayAmount() {
		return totalPayAmount;
	}
	public void setTotalPayAmount(int totalPayAmount) {
		this.totalPayAmount = totalPayAmount;
	}
	
	
}
