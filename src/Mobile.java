//Sherwayne Palmer 738019
//CSAT
public class Mobile extends Bill{

	private String manufacturerName;
	private String planName;
	private String mobileNumber;
	private int GBused;
	private int minuteUsed;
	
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public int getGBused() {
		return GBused;
	}
	public void setGBused(int gBused) {
		GBused = gBused;
	}
	public int getMinuteUsed() {
		return minuteUsed;
	}
	public void setMinuteUsed(int minuteUsed) {
		this.minuteUsed = minuteUsed;
	}
	
}
