//Sherwayne Palmer 738019
//CSAT
public class Hydro extends Bill{

	private String agencyName;
	private int unitConsumed;
	
	
	public String getAgencyName() {
		return agencyName;
	}
	void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	public int getUnitConsumed() {
		return unitConsumed;
	}
	public void setUnitConsumed(int unitConsumed) {
		this.unitConsumed = unitConsumed;
	}
	
	
	
}
